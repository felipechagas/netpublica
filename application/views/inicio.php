<?php 

	isset($_GET["urlDestino"]) ? $urlDestino = $_GET["urlDestino"] : $urlDestino = "inicio";

	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Passado
	header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache"); // HTTP/1.0

?>
        <div class="ctncgt-container-linha-formulario row">
            <div class="ctncgt-container-formulario">
                <?php if ( isset($_GET["msg"]) ) { ?>
                    <div class="ctncgt-container-mensagem row erro">
                      <div class="ctncgt-mensagem-texto col-xs-11 col-sm-11 col-md-11 col-lg-11">CPF ou senha incorretos, tente novamente.</div>
                      <div class="ctncgt-mensagem-fechar col-xs-1 col-sm-1 col-md-1 col-lg-1" ctncgt-mensagem-fechar>x</div>
                    </div>
                <?php } ?>
                <form id="form-terradaluz-login" class="terra-da-fuz-form form-terradaluz-login"
                      action="http://10.175.255.1:8000/" method="POST" accept-charset="utf-8">
                    <div class="ctncgt-container-campo ctncgt-container-campo-borda-lateral">
                        <div class="ctncgt-espacamento-campo-borda-lateral">
                            <input class="ctncgt-campo"  id="auth_user" name="auth_user"
                             type="text" traduz-placeholder="rotulo.placeholder.cpf"
                             placeholder="CPF ou Passaporte" />
                        </div>
                    </div>
                    <div class="ctncgt-container-campo ctncgt-container-campo-borda-lateral">
                        <div class="ctncgt-espacamento-campo-borda-lateral">
                            <input class="ctncgt-campo" id="auth_pass" name="auth_pass"
                             type="password" traduz-placeholder="rotulo.placeholder.senha"
                             placeholder="Senha"/>
                        </div>
                    </div>
                    <div class="ctncgt-container-links row">
                        <div class="ctncgt-container-link">
                            <a class="ctncgt-link" traduz-rotulo="rotulo.cadastre.se" 
                             title="Clique para se cadastrar" 
                             href="<?php echo base_url(); ?>index.php/cadastro">
                                Cadastre-se
                            </a>
                        </div>
                        <div class="ctncgt-container-link">
                            <a class="ctncgt-link" traduz-rotulo="rotulo.esqueceu.senha" 
                             title="Clique para recuperar sua senha" 
                             href="<?php echo base_url(); ?>index.php/cadastro/esqueciSenha">
                                Esqueceu a senha?
                            </a>
                        </div>
                        <div class="ctncgt-container-link">
                            <a class="ctncgt-link" traduz-rotulo="rotulo.reenvie.email.ativacao" 
                             title="Clique para reenviar o e-mail de ativação" 
                             href="<?php echo base_url(); ?>index.php/cadastro/reenviarEmailConfirmacao">
                                Reenvie o e-mail de ativação
                            </a>
                        </div>
                    </div>
                    <div class="ctncgt-container-linha-botoes row">
                        <div class="ctncgt-container-botao">
                            <input title="Entrar no sistema" class="ctncgt-botao" name="accept" 
                             type="submit"
                             traduz-valor="rotulo.valor.entrar" value="Entrar"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
