<?php 
    if($_GET['id'] == NULL || $_GET['email'] == NULL){
        redirect('index.php/page');
    }
?>

<div class="conteiner_formulario_de_login col-sm-offset-2 col-md-offset-3 col-lg-offset-4 col-xs-12 col-sm-8 col-md-6 col-lg-4">
    <form action="<?php echo base_url()?>index.php/cadastro/alterar_senha?id=<?php echo $_GET['id'];?>&email=<?php echo $_GET['email'];?>" method="post" accept-charset="utf-8">
            <?php 
            echo validation_errors(' <div class="alert alert-danger alert-dismissible fade in" '
                    . 'role="alert"><button type="button" class="close" data-dismiss="alert"><span '
                    . 'aria-hidden="true">×</span><span class="sr-only">Close</span></button>','</div>'); ?>
            <div class="form-group">
                <input type="password" class="form-control" traduz-placeholder="rotulo.placeholder.nova.senha" name="senha"  placeholder="Insira a nova senha"/>
            </div>
            <div class="form-group">
                <input type="password" class="form-control" traduz-placeholder="rotulo.placeholder.repetir.nova.senha" name="r_senha" placeholder="Repita a nova senha"/>
            </div>
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                <input class="btn btn-default" type="submit" traduz-valor="rotulo.valor.alterar" value="Alterar"/>
            </div>
        </div>
    </form>
</div>