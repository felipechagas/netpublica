<script type="text/javascript">
  $(document).ready( function() {
    mostrarMensagem(
      'Reenvio do e-mail de confirmação de cadastro',
      'Não foi possível enviar o e-mail para confirmação do seu cadastro, por favor entre em contato com o suporte.',
      '<?php echo base_url(); ?>');
  });
</script>
