<div id="ctncgt-elemento-modal-termos-uso">
	<p>Disciplina o acesso à WiFi PÚBLICO E GRATUITO DA CIDADE DE FORTALEZA e regulamenta suas condições de uso:</p>
    <p>
    	<b>1 - Aceitação do Termo e Condições de Uso</b>
    </p> 
    <p>
    	A aceitação deste Termo e das Condições de Uso é absolutamente indispensável à utilização do acesso sem fio à WiFi PÚBLICO E GRATUITO DA CIDADE DE FORTALEZA, ofertada pelo <b>Programa Fortaleza Inteligente</b> da Prefeitura Municipal de Fortaleza, doravante denominada PREFEITURA DE FORTALEZA.
    </p> 
    <p>
    	Para fazer uso do serviço público e gratuito de acesso à Internet banda larga é preciso:
    </p> 
    <ul>
        <li>Ler e concordar com as condições previstas neste Termo;</li>
        <li>Cadastrar-se, fornecendo informações válidas (no caso de acesso pela modalidade ACESSO AUTENTICADO).</li>
    </ul> 
    <p>
    	<b>2 – Modalidade de Acesso</b>
    </p>
    <p>
    	<b>O Programa Fortaleza Inteligente</b> oferece um serviço de acesso público e gratuito à Internet, interligando computadores, celulares, tablets etc., do USUÁRIO à rede, por meio de conexão sem fio. 
    </p>
    <p>
    	Serão providas duas modalidades de acessos aos cidadãos:
    </p>
    <ul>
        <li>ACESSO RESTRITO – sem necessidade de cadastramento e autenticação, mas restrito a algumas aplicações e serviços básicos da Internet e aos tempos de acesso abaixo definidos. Esta modalidade objetiva permitir o acesso de usuários a suas contas de e-mail para validação do seu cadastro de USUARIO.</li>
        <label>o Tempo de acesso máximo por dia e por ponto de conexão - 10min</label><br>
        <label>o Perda de conexão com 5 min de inatividade</label>
    </ul>
    <ul>
        <li>ACESSO AUTENTICADO – com necessidade de cadastro no primeiro acesso e mediante autenticação, com acesso livre às aplicações e serviços dispostos na Internet, conforme tempos de acesso abaixo definidos. </li>
        <label>o Tempo de acesso máximo por dia - 60min</label>
        <label>o Perda de conexão por inatividade - 10min</label>
    </ul>
    <p><b>3 – Cadastro do USUÁRIO</b></p>
    <p>O USUÁRIO declara e se responsabiliza pela veracidade de todos os dados por ele fornecidos ao <b>Programa Fortaleza Inteligente</b> no momento de seu Cadastro e em qualquer atualização posterior, ficando responsável, portanto, pela sua exatidão e autenticidade.</p>
    <p>No momento do cadastro, o USUÁRIO deverá escolher um nome de USUÁRIO (login) e senha, pessoal e intransferível, por meio dos quais ele terá acesso à rede WiFi Pública e Gratuita da Cidade de Fortaleza, na modalidade ACESSO AUTENTICADO, bem como um endereço de e-mail válido para contato, comprometendo-se a não informá-los a terceiros, responsabilizando-se exclusiva e pessoalmente pelo uso.</p>
    <p>Somente será permitido um único cadastro por USUÁRIO, sendo necessário, para sua efetivação, o preenchimento de todos os campos exigidos no respectivo formulário e a confirmação do mesmo, através de token enviado para o e-mail informado pelo USUÁRIO. Só assim o cadastro ficará ativo e o usuário poderá ter ACESSO AUTENTICADO.</p>
    <p><b>4 – Equipamentos do USUÁRIO</b><p> 
    <p>Para acessar o serviço prestado pelo <b>Programa Fortaleza Inteligente</b> o USUÁRIO deve ter e instalar, às suas expensas e sob sua responsabilidade, qualquer aparelho (notebooks, netbooks, celulares, tablets ou pda’s) compatível com a tecnologia sem fio nos padrões IEEE 802.11 b/g/n, com os softwares de navegação na WEB, bem como deverá promover as medidas de segurança necessárias à proteção de seus equipamentos, sistemas e arquivos contra a atuação indevida e invasões não autorizadas de outros usuários de Internet.</p>
    <p><b>5 – Utilização dos Serviços pelo USUÁRIO</b><p>
    <p>O USUÁRIO se obriga a não utilizar a via de acesso que lhe foi disponibilizada pela PREFEITURA DE FORTALEZA para tentar e conseguir acesso a outros sistemas que não os integrantes da INTERNET e cuja interligação não lhe foi autorizada (redes restritas de órgãos oficiais, etc.).</p> 
    <p>O USUÁRIO se obriga a não utilizar os serviços de modo a prejudicar o acesso de outros USUÁRIOS à INTERNET, e sua livre utilização.</p> 
    <p>O USUÁRIO se compromete a utilizar os serviços prestados pela PREFEITURA DE FORTALEZA, com observância da legislação vigente, somente para fins lícitos. </p>
    <p>A PREFEITURA DE FORTALEZA reserva-se o direito de remover ou recusar-se a veicular qualquer informação detectada que, no todo ou em parte, viole as disposições de uso explicitadas no presente termo.</p> 
    <p>A PREFEITURA DE FORTALEZA poderá suspender, bloquear, cancelar e, ainda, negar-se a disponibilizar acesso à INTERNET – via seu sistema – ao USUÁRIO que violar ou estiver envolvido em violação, direta ou indiretamente, às normas de segurança aqui estabelecidas ou outras normas aplicáveis.</p>
    <p><b>6 – Política de Armazenamento de Registros de Conexão</b></p>
    <p>O USUÁRIO ao se registrar ou enviar informações de login no WiFi Público e Gratuito do <b>Programa Fortaleza Inteligente</b>, fica ciente que o registro de conexão é mantido em bancos de dados pelo prazo de (01) um ano, conforme exigido no Art.13 do Marco Civil da Internet, Lei Nº 12.965, de 23 de abril de 2014.</p>
    <p>Ao solicitar a exclusão da sua conta, ela será desativada mas os dados cadastrais e os registros de conexão serão mantidos pelo prazo de (01) um ano, conforme exigido no Art.13 do Marco Civil da Internet, Lei Nº 12.965, de 23 de abril de 2014.</p>
    <p>A senha do USUÁRIO é armazenada com criptografia irreversível (<b>SHA1/MD5</b>), sendo assim não é possível sua restauração. Desse modo a senha do USUÁRIO só pode ser redefinida, sendo essa redefinição encaminhada apenas ao seu e-mail.</p>
    <p><b>7-  Prazo do presente termo de uso</b></p>
    <p>O presente Termo entra em vigor a partir da aceitação eletrônica ratificada pelo Usuário, permanecendo vigente por prazo indeterminado até que qualquer das Partes motive a rescisão contratual nas formas definidas neste Instrumento.</p>
    <p><b>8 – Disposições Gerais</b></p>
    <p>Este Termo regulamenta a Política de Uso e Privacidade do WiFi PÚBLICO E GRATUITO DA CIDADE DE FORTALEZA (Clique aqui para acessar).</p>
    <p>O USUÁRIO declara que possui plena capacidade jurídica para celebrar o presente contrato com a PREFEITURA DE FORTALEZA e declara ter compreendido todos os termos e condições contratuais, aceitando-os sem reservas ou ressalvas e, obrigando-se a respeitá-los e cumpri-los.</p>
    <p>As expressões em outro idioma utilizadas no texto deste regulamento são as internacionalmente consagradas para as atividades e serviços nele contemplados, devendo ser lidas e interpretadas de acordo com o significado que lhes é atribuído pela comunidade internacional.</p>
    <p>O não exercício pelas partes de quaisquer direitos previstos neste regulamento representa mera liberalidade, não implicando em renúncia, novação e/ou transação relativamente a tais direitos, os quais poderão ser exercidos a qualquer momento.</p>
    <p>Se qualquer das disposições deste regulamento vier a ser considerada ilegal, inválida ou ineficaz por expressa previsão em lei posterior a sua formalização ou por decisão administrativa ou judicial, tal ilegalidade, falta de validade ou ineficácia será interpretada restritivamente, não prejudicando o contrato como um todo, que continuará vigente com todas as suas demais estipulações.</p>
    <p>As partes elegem o Foro da cidade de Fortaleza, Estado do Ceará, com renúncia a qualquer outro, por mais privilegiado que seja ou venha a ser, para dirimir as questões deste regulamento.</p>
    <p><b>COORDENADORIA DE CIÊNCIA, TECNOLOGIA E INOVAÇÃO PREFEITURA MUNICIPAL DE FORTALEZA</b></p>
</div>