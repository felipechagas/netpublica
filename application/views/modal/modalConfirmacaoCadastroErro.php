<script type="text/javascript">
	$(document).ready( function() {
		mostrarMensagem(
			'E-mail não enviado',
			'Não foi possível enviar o e-mail para confirmação do seu cadastro, aperte <a href="<?php echo base_url();?>index.php/cadastro/reenviarEmailConfirmacao">aqui</a> para reenviar o e-mail.',
			'http://fortalezainteligente.fortaleza.ce.gov.br');
	});
</script>
