<div id="ctncgt-elemento-modal-sobre">
	<img class="ctncgt-imagem-modal-sobre img-responsive" src="<?php echo base_url(); ?>imagens/logo-invertida-wifi.jpg"/>
	<span>
		A prefeitura de Fortaleza, dando andamento ao programa Fortaleza Inteligente, 
		disponibiliza para a população, acesso Wi-Fi gratuito à Internet em logradouros públicos de
		grande circulação como praças e terminais de ônibus. A iniciativa visa promover conectividade e
		inclusão digital em Fortaleza.  O Projeto de implantação se realizará em fases, nas quais novos
		grupos de logradouros serão inseridos no programa.
		Estão a frente da iniciativa a Coordenadoria de Ciência Tecnologia e Inovação – 
		CITINOVA e a Secretaria de Planejamento Orçamento de Gestão, por meio de sua Coordenadoria de
		Gestão Corporativa de Tecnologia da Informação – COGECT.
	</span>
    <div class="ctncgt-container-modal-sobre col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <span traduz-rotulo="rotulo.leia.termos.uso">Leia os termos de uso </span>

        <a href="<?php echo base_url(); ?>index.php/termos_de_uso" traduz-rotulo="rotulo.clicando.aqui">
            clicando aqui!
        </a>
    </div>
</div>
