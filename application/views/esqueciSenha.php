<div class="ctncgt-container-linha-formulario row">
    <div class="ctncgt-container-formulario">
        <form action="<?php echo base_url()?>index.php/cadastro/esqueciSenha" method="POST" accept-charset="utf-8">
            <?php if (!empty($this->session->flashdata('emailNaoCadastrado'))) { ?>
                <div class="ctncgt-container-mensagem row erro">
                    <div class="ctncgt-mensagem-texto col-xs-11 col-sm-11 col-md-11 col-lg-11"><?php echo $this->session->flashdata('emailNaoCadastrado'); ?></div>
                    <div class="ctncgt-mensagem-fechar col-xs-1 col-sm-1 col-md-1 col-lg-1" ctncgt-mensagem-fechar>x</div>
                </div>
            <?php } ?>
            <?php if (!empty($this->session->flashdata('usuarioNaoAtivo'))) { ?>
                <div class="ctncgt-container-mensagem row erro">
                    <div class="ctncgt-mensagem-texto col-xs-11 col-sm-11 col-md-11 col-lg-11"><?php echo $this->session->flashdata('usuarioNaoAtivo'); ?></div>
                    <div class="ctncgt-mensagem-fechar col-xs-1 col-sm-1 col-md-1 col-lg-1" ctncgt-mensagem-fechar>x</div>
                </div>
            <?php } ?>
            <?php if (!empty(form_error('email'))) { ?>
                <div class="ctncgt-container-mensagem row erro">
                    <div class="ctncgt-mensagem-texto col-xs-11 col-sm-11 col-md-11 col-lg-11"><?php echo form_error('email'); ?></div>
                    <div class="ctncgt-mensagem-fechar col-xs-1 col-sm-1 col-md-1 col-lg-1" ctncgt-mensagem-fechar>x</div>
                </div>
            <?php } ?>
            <div class="ctncgt-container-campo ctncgt-container-campo-borda-lateral">
                <div class="ctncgt-espacamento-campo-borda-lateral">
                    <input class="ctncgt-campo" name="email"
                     type="email" traduz-placeholder="rotulo.placeholder.email.esqueci.senha"
                     placeholder="Insira seu e-mail"/>
                </div>
            </div>
            <div class="ctncgt-container-links row">
                <div class="ctncgt-container-link">
                    <a class="ctncgt-link" traduz-rotulo="rotulo.primeira.vez" 
                     href="<?php echo base_url(); ?>index.php/cadastro">
                        Primeira vez aqui?
                    </a>
                </div>
                <div class="ctncgt-container-link">
                    <a class="ctncgt-link" traduz-rotulo="rotulo.ja.cadastrado" 
                     href="<?php echo base_url(); ?>index.php/page">
                        Já é cadastrado?
                    </a>
                </div>
            </div>
            <div class="ctncgt-container-linha-botoes row">
                <div class="ctncgt-container-botao">
                    <button title="Entrar no sistema" class="ctncgt-botao"
                     traduz-rotulo="rotulo.valor.enviar">
                        Enviar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
