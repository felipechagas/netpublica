<div class="ctncgt-container-linha-menu row">
    <div class="ctncgt-container-sobre pull-left">
        <a id="ctncgt-elemento-botao-sobre" class="ctncgt-botao" 
         ctncgt-funcao-modal="ctncgt-elemento-modal-sobre" 
         traduz-dica="rotulo.dica.sobre"
         traduz-rotulo="rotulo.sobre">
        	Sobre
        </a>
    </div>
    <div id="elemento-container-idiomas" class="ctncgt-container-idiomas pull-right">
        <input id="idioma-escolhido" type="hidden" value="pt-br"/>

        <img id="elemento-idioma-pt-br" idioma="pt-br" traduz-alt="rotulo.alternativo.idioma.pt.br" 
         src="<?php echo base_url(); ?>imagens/idioma-pt-br.jpg" class="ctncgt-idioma-detacado"/>
        <img id="elemento-idioma-en-us" idioma="en-us" traduz-alt="rotulo.alternativo.idioma.en.us" 
         src="<?php echo base_url(); ?>imagens/idioma-en-us.jpg"/>
    </div>
</div>
