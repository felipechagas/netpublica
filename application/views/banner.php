<html lang="pt">
<head>
    <title traduz-rotulo="rotulo.aplicacao.nome">Fortaleza Inteligente - Wi-Fi Público Gratuita</title>

    <meta http-equiv="Content-Type" content="text/html; charset= utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="<?php echo base_url(); ?>imagens/fav-ico.jpg" type="image/x-icon">

    <link href="<?php echo base_url(); ?>css/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>css/bootstrap/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>

    <link href="<?php echo base_url(); ?>css/centro/geral.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>css/template/banner.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>css/centro/menu.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>css/template/mensagens.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>css/centro/inicio.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>css/centro/cadastro.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>css/template/ctncgt-modal.css" rel="stylesheet" type="text/css"/>

    <link href="<?php echo base_url(); ?>css/centro/especial.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>css/mobile/mobile-landscape.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url(); ?>css/mobile/mobile-portrait.css" rel="stylesheet" type="text/css"/>

    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/geral.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/idioma/idioma.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/ctncgt-modal.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/mensagens.js"></script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-57973795-5', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
    <div class="container">
        <div class="ctncgt-container-cabecalho row">
            <div class="col-xs-offset-1 col-sm-offset-1 col-md-offset-4
                        col-xs-10 col-sm-10 col-md-4 col-lg-4">
                <div class="ctncgt-container-logos row">
                    <img alt="Logo - Prefeitura de Fortaleza, Programa Fortaleza Inteligente e Wifi Público" class="img-responsive" 
                     title="Fortaleza Inteligente - Wi-Fi Público Gratuita" src="<?php echo base_url(); ?>imagens/logo.jpg"/>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="ctncgt-container-central
                        col-xs-offset-1 col-sm-offset-1 col-md-offset-3 col-lg-offset-4
                        col-xs-10 col-sm-10 col-md-6 col-lg-4">
