<script>
    $(function(){
        $('#new_captcha').click( function(event){
            event.preventDefault();
            
            $("#img_captcha img").attr('src', 'cadastro/new_captcha?'+Math.random());
        });
    });
</script>

<div class="ctncgt-container-linha-formulario row">
    <div class="ctncgt-container-formulario">
        <div class="row">
            <?php if (!empty($erro)) { echo '<script>mostrarErro("' . form_error('nome') . '");</script>'; } ?>
        </div>
        <div class="ctncgt-container-destaque row">
            <div class="ctncgt-destaque">
                <span traduz-rotulo="rotulo.todos.os.campos.obrigatorios">
                    Todos os campos abaixo são obrigatórios
                </span>
            </div>
        </div>

        <form action="<?php echo base_url()?>index.php/cadastro" method="POST" accept-charset="utf-8">
            <!-- NOME -->
            <?php if (!empty(form_error('nome'))) { ?>
                <div class="ctncgt-container-mensagem row erro">
                    <div class="ctncgt-mensagem-texto col-xs-11 col-sm-11 col-md-11 col-lg-11"><?php echo form_error('nome'); ?></div>
                    <div class="ctncgt-mensagem-fechar col-xs-1 col-sm-1 col-md-1 col-lg-1" ctncgt-mensagem-fechar>x</div>
                </div>
            <?php } ?>
            <div class="ctncgt-container-campo ctncgt-container-campo-borda-lateral">
                <div class="ctncgt-espacamento-campo-borda-lateral">
                    <input class="ctncgt-campo" name="nome" type="text" autocomplete="off" value="<?php echo set_value('nome') ?>" 
                     traduz-placeholder="rotulo.placeholder.nome" placeholder="Nome"/>
                </div>
            </div>
            <!-- DOCUMENTO -->
            <?php if (!empty(form_error('documento'))) { ?>
                <div class="ctncgt-container-mensagem row erro">
                    <div class="ctncgt-mensagem-texto col-xs-11 col-sm-11 col-md-11 col-lg-11"><?php echo form_error('documento'); ?></div>
                    <div class="ctncgt-mensagem-fechar col-xs-1 col-sm-1 col-md-1 col-lg-1" ctncgt-mensagem-fechar>x</div>
                </div>
            <?php } ?>
            <div class="ctncgt-container-campo ctncgt-container-campo-borda-lateral">
                <div class="ctncgt-espacamento-campo-borda-lateral">
                    <input class="ctncgt-campo" name="documento" type="text" autocomplete="off" value="<?php echo set_value('documento') ?>"
                     traduz-placeholder="rotulo.placeholder.cpf" placeholder="CPF"/>
                </div>
            </div>
            <!-- EMAIL -->
            <?php if (!empty(form_error('email'))) { ?>
                <div class="ctncgt-container-mensagem row erro">
                    <div class="ctncgt-mensagem-texto col-xs-11 col-sm-11 col-md-11 col-lg-11"><?php echo form_error('email'); ?></div>
                    <div class="ctncgt-mensagem-fechar col-xs-1 col-sm-1 col-md-1 col-lg-1" ctncgt-mensagem-fechar>x</div>
                </div>
            <?php } ?>
            <div class="ctncgt-container-campo ctncgt-container-campo-borda-lateral">
                <div class="ctncgt-espacamento-campo-borda-lateral">
                    <input class="ctncgt-campo" name="email" type="email" autocomplete="off" value="<?php echo set_value('email') ?>"
                     traduz-placeholder="rotulo.placeholder.email" placeholder="E-Mail"/>
                </div>
            </div>
            <!-- DATA -->
            <?php if (!empty(form_error('data_nascimento'))) { ?>
                <div class="ctncgt-container-mensagem row erro">
                    <div class="ctncgt-mensagem-texto col-xs-11 col-sm-11 col-md-11 col-lg-11"><?php echo form_error('data_nascimento'); ?></div>
                    <div class="ctncgt-mensagem-fechar col-xs-1 col-sm-1 col-md-1 col-lg-1" ctncgt-mensagem-fechar>x</div>
                </div>
            <?php } ?>
            <div class="ctncgt-container-label-nascimento row">
                <span traduz-rotulo="rotulo.data.nascimento">
                    Data de nascimento :
                </span>
            </div>
            <div class="ctncgt-container-dropboxies row">
                <!-- DIAS -->
                <div class="ctncgt-container-dropbox-dias col-xs-3 col-sm-3 col-md-4 col-lg-4">
                    <select class="ctncgt-dropbox-dias" name="data_nascimento[]">
                        <?php for ($indiceDias = 1; $indiceDias <= 31; $indiceDias++) { ?>
                            <option <?php echo ( ($this->input->post()['data_nascimento'][0] == $indiceDias) ? 'selected' : '' )  ?>>
                                <?php echo sprintf("%02d", $indiceDias); ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <!-- MESES -->
                <div class="ctncgt-container-dropbox-meses col-xs-5 col-sm-5 col-md-4 col-lg-4">
                    <select class="ctncgt-dropbox-meses" name="data_nascimento[]">
                        <?php for ($indiceMeses = 1; $indiceMeses <= 12; $indiceMeses++) { ?>
                            <option <?php echo ( ($this->input->post()['data_nascimento'][1] == $indiceMeses) ? 'selected' : '' )  ?>
                             traduz-rotulo="rotulo.mes.<?php echo $indiceMeses; ?>" value="<?php echo $indiceMeses; ?>">
                                <?php echo date('F', strtotime("$indiceMeses/01/2000")); ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
                <!-- ANOS -->
                <div class="ctncgt-container-dropbox-anos col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <select class="ctncgt-dropbox-anos" name="data_nascimento[]">
                        <?php for ($indiceAnos = ( (int) date('Y') ); $indiceAnos >= 1900 ; $indiceAnos--) { ?>
                            <option <?php echo ( ($this->input->post()['data_nascimento'][2] == $indiceAnos) ? 'selected' : '' )  ?>
                             traduz-rotulo="rotulo.mes.<?php echo $indiceAnos; ?>" value="<?php echo $indiceAnos; ?>">
                                <?php echo $indiceAnos; ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <!-- SENHA -->
            <?php if (!empty(form_error('senha'))) { ?>
                <div class="ctncgt-container-mensagem row erro">
                    <div class="ctncgt-mensagem-texto col-xs-11 col-sm-11 col-md-11 col-lg-11"><?php echo form_error('senha'); ?></div>
                    <div class="ctncgt-mensagem-fechar col-xs-1 col-sm-1 col-md-1 col-lg-1" ctncgt-mensagem-fechar>x</div>
                </div>
            <?php } ?>
            <div class="ctncgt-container-campo ctncgt-container-campo-borda-lateral">
                <div class="ctncgt-espacamento-campo-borda-lateral">
                    <input class="ctncgt-campo" name="senha" type="password" value=""
                     traduz-placeholder="rotulo.placeholder.digite.senha" placeholder="Digite sua Senha"/>
                </div>
            </div>
            <!-- CONFIRMAR SENHA -->
            <?php if (!empty(form_error('confirmar_senha'))) { ?>
                <div class="ctncgt-container-mensagem row erro">
                    <div class="ctncgt-mensagem-texto col-xs-11 col-sm-11 col-md-11 col-lg-11"><?php echo form_error('confirmar_senha'); ?></div>
                    <div class="ctncgt-mensagem-fechar col-xs-1 col-sm-1 col-md-1 col-lg-1" ctncgt-mensagem-fechar>x</div>
                </div>
            <?php } ?>
            <div class="ctncgt-container-campo ctncgt-container-campo-borda-lateral">
                <div class="ctncgt-espacamento-campo-borda-lateral">
                    <input class="ctncgt-campo"  name="confirmar_senha" type="password" value=""
                     traduz-placeholder="rotulo.placeholder.confirmar.senha" placeholder="Confirme sua Senha"/>
                </div>
            </div>
            <!-- CAPTCHA -->
            <?php if (!empty(form_error('captcha'))) { ?>
                <div class="ctncgt-container-mensagem row erro">
                    <div class="ctncgt-mensagem-texto col-xs-11 col-sm-11 col-md-11 col-lg-11"><?php echo form_error('captcha'); ?></div>
                    <div class="ctncgt-mensagem-fechar col-xs-1 col-sm-1 col-md-1 col-lg-1" ctncgt-mensagem-fechar>x</div>
                </div>
            <?php } ?>
            <div class="ctncgt-container-captcha row">
                <div id="img_captcha" class="ctncgt-container-captcha-imagem col-xs-9 col-sm-9 col-md-8 col-lg-8">
                    <?php echo $captcha; ?>
                </div>
               <div class="ctncgt-container-recarrega-captcha col-xs-3 col-sm-3 col-md-4 col-lg-4">
                    <div id="new_captcha" class="ctncgt-sub-container-recarrega-captcha">
                        <img alt="Recarregar Captcha" 
                         src="<?php echo base_url(); ?>imagens/recarrega-icon.jpg"/>
                    </div>
                </div>
            </div>
            <div class="ctncgt-container-campo-captcha row">
                <div class="ctncgt-container-campo ctncgt-container-campo-borda-lateral">
                    <div class="ctncgt-espacamento-campo-borda-lateral">
                        <input id="ctncgt-captcha" class="ctncgt-campo" name="captcha" type="text" 
                         traduz-placeholder="rotulo.placeholder.repita.caracteres" autocomplete="off" placeholder="Repita os Caracteres acima"/>
                    </div>
                </div>
            </div>
            <!-- TERMOS -->
            <?php if (!empty(form_error('termos_uso'))) { ?>
                <div class="ctncgt-container-mensagem row erro">
                    <div class="ctncgt-mensagem-texto col-xs-11 col-sm-11 col-md-11 col-lg-11"><?php echo form_error('termos_uso'); ?></div>
                    <div class="ctncgt-mensagem-fechar col-xs-1 col-sm-1 col-md-1 col-lg-1" ctncgt-mensagem-fechar>x</div>
                </div>
            <?php } ?>
            <div class="ctncgt-container-termos row">
                <input id="elemento-checkbox" type="checkbox" name="termos_uso" value="Sim" />
                <label for="elemento-checkbox" traduz-rotulo="rotulo.concordo.termos">Concordo com os</label>
                <a type="button" class="ctncgt-botao-link" traduz-rotulo="rotulo.termos.uso" ctncgt-funcao-modal="ctncgt-elemento-modal-termos-uso">
                    Termos de Uso
                </a>
            </div>
            <div class="ctncgt-container-linha-botoes row">
                <div class="ctncgt-container-botao">
                    <button title="Entrar no sistema" class="ctncgt-botao" traduz-rotulo="rotulo.valor.cadastrar">
                        Cadastrar
                    </button>
                </div>
            </div>
        </form>
        <div class="ctncgt-container-links row">
            <div class="ctncgt-container-link">
                <a class="ctncgt-link" traduz-rotulo="rotulo.ja.cadastrado" 
                 href="<?php echo base_url(); ?>index.php/page">
                    Já é cadastrado?
                </a>
            </div>
        </div>
    </div>
</div>
