<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cadastro extends CI_Controller {
    public $codificacao = "<meta http-equiv='Content-Type' content='text/html; charset= utf-8 (Sem BOM)' />";

    function __construct(){
        parent::__construct();
        $this->load->library('My_PHPMailer');
        $this->load->library('Api_Cogect');
        $this->load->library('encrypt');
        $this->load->database();
        $this->load->library('mycaptcha');
        date_default_timezone_set('America/Fortaleza');
    }
    
    public function index(){
        $this->form_validation->set_rules('nome','Nome','required');
        $this->form_validation->set_rules('documento','Documento','required|callback_validacao_cpf');
        $this->form_validation->set_rules('data_nascimento','Data de Nascimento','callback_validacao_data_nascimento');
        $this->form_validation->set_message('is_unique','O %s já está cadastro em nosso sistema');
        $this->form_validation->set_rules('email','E-mail','required|valid_email|is_unique[usuario.email]');
        $this->form_validation->set_rules('senha','Senha','required');
        $this->form_validation->set_rules('confirmar_senha','Confirmar Senha','required|matches[senha]');
        $this->form_validation->set_rules('captcha', 'Captcha', 'required|validate_captcha'); 
        $this->form_validation->set_rules('termos_uso','Termos de Uso','required');
        if($this->form_validation->run() == FALSE){
            $data['captcha'] = $this->mycaptcha
                ->deleteImage()
                ->createWord()
                ->createCaptcha();
            $this->load->view('banner');
            $this->load->view('menu');
            $this->load->view('mensagens');
            $this->load->view('cadastro', $data);
            $this->load->view('modal/modalSobre');
            $this->load->view('modal/modalTermosUso');
            $this->load->view('footer');
        }
        else{
            $dados_cadastrais = elements( array('nome','documento','data_nascimento','email', 'senha'), $this->input->post() );
            $dados_cadastrais['data_nascimento'] = $dados_cadastrais['data_nascimento'][2].'-'.$dados_cadastrais['data_nascimento'][1].'-'.$dados_cadastrais['data_nascimento'][0];
            $dados_cadastrais['status'] = '2';
            $dados_cadastrais['documento'] = str_replace(".", "", $dados_cadastrais['documento']);
            $dados_cadastrais['documento'] = str_replace("-", "", $dados_cadastrais['documento']);
            $dados_cadastrais['data_cadastro'] = date('Y-m-d');
            //$dados_cadastrais['senha']= $this->encrypt->encode($dados_cadastrais['senha']);
            $dados_cadastrais['senha']= $dados_cadastrais['senha'];
            
            $this->gerenciador->cadastrar($dados_cadastrais);

            $idUser=$this->gerenciador->verificaEmail($dados_cadastrais['email'])->row();
            
            $mail = new PHPMailer;
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.fortaleza.ce.gov.br';             // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'freewifi@fortaleza.ce.gov.br';     // SMTP username
            $mail->Password = 'xfJX3G@J.7P_';                     // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            $mail->From = 'freewifi@fortaleza.ce.gov.br';
            $mail->FromName = 'Prefeitura Municipal de Fortaleza';
            $mail->addAddress($this->input->post('email'), $this->input->post('nome'));   // Add a recipient
            $mail->addReplyTo('freewifi@fortaleza.ce.gov.br', 'Suporte Wifi');

            $mail->isHTML(true);                                  // Set email format to HTML
            
            $mail->CharSet = 'utf8';

            $mail->Subject = 'Confirmação de Cadastro do Wifi Praças Públicas - Prefeitura Municipal de Fortaleza';
            $mail->Body    = 'Por favor confirme o seu cadastro no Wifi Publico Gratuito clicando no link a seguir:'
                    . '<br>'
                    . '<a href="http://wifi.fortaleza.ce.gov.br/index.php/cadastro/ativacao?id='.$idUser->id.'&email='.md5($dados_cadastrais['email']).'">http://wifi.fortaleza.ce.gov.br/index.php/cadastro/ativacao?id='.$idUser->id.'&email='.md5($dados_cadastrais['email']).'</a>'
                    . '<br>'
                    . '<br>'
                    . 'Please confirm your registration in the Public Wifi Free by clicking the following link:'
                    . '<br>'
                    . '<a href="http://wifi.fortaleza.ce.gov.br/index.php/cadastro/ativacao?id='.$idUser->id.'&email='.md5($dados_cadastrais['email']).'">http://wifi.fortaleza.ce.gov.br/index.php/cadastro/ativacao?id='.$idUser->id.'&email='.md5($dados_cadastrais['email']).'</a>'
                    . '<br>'
                    . '<br>'
                    . 'OBS : Caso não consiga abrir o link, copie e cole no campo do navegador.'
            ;

            if(!$mail->send()) {
                $this->load->view('banner');
                $this->load->view('menu');
                $this->load->view('modal/modalEnviarEmailCadastroErro');
                $this->load->view('footer');
            } else {
                $this->load->view('banner');
                $this->load->view('menu');
                $this->load->view('modal/modalEnviarEmailCadastro');
                $this->load->view('modal/modalSobre');
                $this->load->view('footer');
            }
        }
    }
    
    public function validacao_cpf($documento){
        $documento = str_replace(".", "", $documento);
        $documento = str_replace("-", "", $documento);
        if(ctype_digit($documento)==TRUE){
            if(strlen($documento) == 11){
                if ($documento == '00000000000' || $documento == '11111111111' || $documento == '22222222222' || 
                    $documento == '33333333333' || $documento == '44444444444' || $documento == '55555555555' || 
                    $documento == '66666666666' || $documento == '77777777777' || $documento == '88888888888' || 
                    $documento == '99999999999'){
                    $this->form_validation->set_message('validacao_cpf', 'O CPF digitado não é válido');
                    return false;
                }
                else{
                    for ($t = 9; $t < 11; $t++) {
                        for ($d = 0, $c = 0; $c < $t; $c++) {
                            $d += $documento{$c} * (($t + 1) - $c);
                        }
                        $d = ((10 * $d) % 11) % 10;
                    }
                    if ($documento{$c} != $d) {
                        $this->form_validation->set_message('validacao_cpf', 'O CPF digitado não é válido');
                        return false;
                    }
                    $resultado=$this->gerenciador->verifica_cpf($documento);
                    if($resultado == NULL){
                        return TRUE;
                    }
                    else{
                        $this->form_validation->set_message('validacao_cpf', 'O CPF já está cadastrado no nosso sistema');
                        return false;
                    }
                }
            }
            else{
                $this->form_validation->set_message('validacao_cpf', 'O CPF deve conter somente 11 números');
                return false;
            }
        }
        else{
            $resultado=$this->gerenciador->verifica_cpf($documento);
            if($resultado == NULL){
                return TRUE;
            }
            else{
                $this->form_validation->set_message('validacao_cpf', 'O Documento já está cadastrado no nosso sistema');
                return false;
            }
        }
    }

    public function validacao_data_nascimento($data_nascimento) {
        if (checkdate($data_nascimento[1], $data_nascimento[0], $data_nascimento[2])) {
            return TRUE;
        } else {
            $this->form_validation->set_message('validacao_data_nascimento', 'Escolha uma data válida.');

            return FALSE;
        }
    }
    
    public function esqueciSenha(){
        $this->form_validation->set_rules('email','E-mail','required|valid_email');
        if($this->form_validation->run() == FALSE){
            $this->load->view('banner');
            $this->load->view('menu');
            $this->load->view('mensagens');
            $this->load->view('esqueciSenha');
            $this->load->view('modal/modalSobre');
            $this->load->view('footer');
        }
        else{
            if($this->gerenciador->verificaEmail($this->input->post('email'))->num_rows == 0){
                $this->session->set_flashdata('emailNaoCadastrado','O E-mail que você digitou não está cadastrado');
                redirect('index.php/cadastro/esqueciSenha');
            }
            if($this->gerenciador->verificaSeAtivo($this->input->post('email'))->status==2){
                $this->session->set_flashdata('usuarioNaoAtivo','Você ainda não ativou o seu cadastro, por favor verifique seu e-mail para fazer a ativação.');
                redirect('index.php/cadastro/esqueciSenha');
            }
            else{
                $dados = $this->gerenciador->verificaEmail($this->input->post('email'))->row();
                $mail = new PHPMailer;
                $mail->isSMTP();                                      // Set mailer to use SMTP
                $mail->Host = 'smtp.fortaleza.ce.gov.br';             // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                               // Enable SMTP authentication
                $mail->Username = 'freewifi@fortaleza.ce.gov.br';     // SMTP username
                $mail->Password = 'xfJX3G@J.7P_';                     // SMTP password
                $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 587;                                    // TCP port to connect to

                $mail->From = 'freewifi@fortaleza.ce.gov.br';
                $mail->FromName = 'Prefeitura Municipal de Fortaleza';
                $mail->addAddress($this->input->post('email'), $dados->nome);   // Add a recipient
                $mail->addReplyTo('freewifi@fortaleza.ce.gov.br', 'Suporte Wifi');

                $mail->isHTML(true);                                  // Set email format to HTML
                
                $mail->CharSet = 'utf8';

                $mail->Subject = 'Recuperação de Senha Wifi Praças Publicas de Fortaleza';
                $mail->Body    = 'Clique no link para alterar a sua senha: http://wifi.fortaleza.ce.gov.br/index.php/cadastro/alterar_senha?id='.$dados->id.'&email='.md5($dados->email).'<br>'
                        . '<a href="http://wifi.fortaleza.ce.gov.br/index.php/cadastro/alterar_senha?id='.$dados->id.'&email='.md5($dados->email).'">Ou clique aqui.</a><br><br>'
                        . 'Click the link to change your password in Public Wifi Free: http://wifi.fortaleza.ce.gov.br/index.php/cadastro/alterar_senha?id='.$dados->id.'&email='.md5($dados->email).'<br>'
                        . '<a href="http://wifi.fortaleza.ce.gov.br/index.php/cadastro/alterar_senha?id='.$dados->id.'&email='.md5($dados->email).'">Or click here.</a>';

                if(!$mail->send()) {
                    $this->load->view('banner');
                    $this->load->view('menu');
                    $this->load->view('modal/modalEnviarEmailEsqueciSenhaErro');
                    $this->load->view('modal/modalSobre');
                    $this->load->view('footer');
                } else {
                    $this->load->view('banner');
                    $this->load->view('menu');
                    $this->load->view('modal/modalEnviarEmailEsqueciSenha');
                    $this->load->view('modal/modalSobre');
                    $this->load->view('footer');
                }
            }
        }
    }
    
    public function ativacao(){
        $emailmd5 = $_GET['email'];
        $id = $_GET['id'];
        $copiou = $this->copiar_para_cogect($id);
        if($copiou) {
            $ativou = $this->gerenciador->ativarCadastro($id,$emailmd5);
            if($ativou) {
                $this->load->view('banner');
                $this->load->view('menu');
                $this->load->view('modal/modalConfirmacaoCadastro');
                $this->load->view('modal/modalSobre');
                $this->load->view('footer');
            }
        }
        else{
            $this->load->view('banner');
                $this->load->view('menu');
                $this->load->view('modal/modalConfirmacaoCadastroErro');
                $this->load->view('modal/modalSobre');
                $this->load->view('footer');
        }
    }

    public function copiar_para_cogect($id){
        $host = "192.168.2.29";
    	$porta = "389";
    	$basedn = "dc=fortaleza,dc=ce,dc=gov,dc=br";
    	$admin = "Manager";
    	$senhaAdmin = "azsxdcfv";
    	$conexao = NULL;
        $dados_usuario = $this->gerenciador->get_usuario($id);
        //$dados_usuario['senha'] = $this->encrypt->decode($dados_usuario['senha']);
        $ldapManager = new ldapManager($host,$porta,$basedn,$admin,$senhaAdmin);
        $conexao = $ldapManager->getConnectLdap();
       
        if(!$conexao) {
            echo $this->codificacao."<script>alert('Conexão indisponível, tente confirmar novamente em seu e-mail!');
                    window.location.assign('http://wifi.fortaleza.ce.gov.br/');</script>";
            return false;
        }
        $ja_cadastrado = $ldapManager->validateUIDUserLdap($conexao, $dados_usuario['documento']);
        if($ja_cadastrado) {
            echo $this->codificacao."<script>alert('Documento já cadastrado!');
                    window.location.assign('http://wifi.fortaleza.ce.gov.br/');</script>";
            return false;
        }
        if($dados_usuario['documento']) $dados_usuario['tipo_documento'] = 'CPF';
        else $dados_usuario['tipo_documento'] = 'passaporte';

        $salvo = $ldapManager->userAddLdap($conexao, $dados_usuario['nome'], $dados_usuario['documento'], $dados_usuario['data_nascimento'], $dados_usuario['email'], $dados_usuario['senha'], $dados_usuario['tipo_documento']);
        if(!$conexao) {
            echo $this->codificacao."<script>alert('Conexão indisponível, tente confirmar novamente em seu e-mail!');
                    window.location.assign('http://wifi.fortaleza.ce.gov.br/');</script>";
            return false;
        }
        $ldapManager->closeConnectLdap($conexao);
        return true;
    }
    
    public function alterar_senha(){
        $emailmd5 = $_GET['email'];
        $id = $_GET['id'];
        $this->form_validation->set_rules('senha','Senha','required');
        $this->form_validation->set_rules('r_senha','Repetir senha','required|matches[senha]');
        if($this->form_validation->run() == FALSE){
            $this->load->view('banner');
            $this->load->view('menu');
            $this->load->view('alterar_senha');
            $this->load->view('modal/modalSobre');
            $this->load->view('footer');
        }
        else{
            $senha = $this->input->post('senha');
            if($this->gerenciador->alterar_senha($id,$emailmd5,$senha) && $this->alterar_senha_cogect($id, $senha)){
                $this->load->view('banner');
                $this->load->view('menu');
                $this->load->view('modal/modalAlteracaoSenha');
                $this->load->view('modal/modalSobre');
                $this->load->view('footer');
            }
            else{
                $this->load->view('banner');
                $this->load->view('menu');
                $this->load->view('modal/modalAlteracaoSenhaErro');
                $this->load->view('modal/modalSobre');
                $this->load->view('footer');
            }
        }
    }

    public function alterar_senha_cogect($id, $senha){
        $host = "192.168.2.29";
        $porta = "389";
        $basedn = "dc=fortaleza,dc=ce,dc=gov,dc=br";
        $admin = "Manager";
        $senhaAdmin = "azsxdcfv";
        $conexao = NULL;
        $dados_usuario = $this->gerenciador->get_usuario($id);
        $ldapManager = new ldapManager($host,$porta,$basedn,$admin,$senhaAdmin);
        $conexao = $ldapManager->getConnectLdap();
        if(!$conexao) {
            echo $this->codificacao."<script>alert('Conexão indisponível, tente novamente!');
                    window.location.assign('http://wifi.fortaleza.ce.gov.br/');</script>";
            return false;
        }
        $ldapManager->userModifyPasswordLdap($conexao, $dados_usuario['documento'], $senha);
        $ldapManager->closeConnectLdap($conexao);
        
        return TRUE;
    }
    
    public function reenviarEmailConfirmacao() {
        $this->form_validation->set_rules('email','E-mail','required|valid_email');
        $this->form_validation->set_rules('r_email','Repitir e-mail','required|valid_email|matches[email]');
        if($this->form_validation->run() == FALSE){
            $this->load->view('banner');
            $this->load->view('menu');
            $this->load->view('reenviarEmail');
            $this->load->view('modal/modalSobre');
            $this->load->view('footer');
        }
        else{
            if($this->gerenciador->verificaEmail($this->input->post('email'))->num_rows == 0){
                $this->session->set_flashdata('emailNaoCadastrado','O E-mail que você digitou não está cadastrado');
                redirect('index.php/cadastro/reenviarEmailConfirmacao');
            }
            if($this->gerenciador->verificaSeAtivo($this->input->post('email'))->status==1){
                $this->session->set_flashdata('emailAtivo','Este cadastro já foi ativado! Se você esqueceu sua senha, <a href="' . base_url() . 'index.php/page/esqueciSenha">Clique aqui!</a>');
                redirect('index.php/cadastro/reenviarEmailConfirmacao');
            }
            $idUser=$this->gerenciador->verificaEmail($this->input->post('email'))->row();
            
            $mail = new PHPMailer;
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'smtp.fortaleza.ce.gov.br';             // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'freewifi@fortaleza.ce.gov.br';     // SMTP username
            $mail->Password = 'xfJX3G@J.7P_';                     // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                                    // TCP port to connect to

            $mail->From = 'freewifi@fortaleza.ce.gov.br';
            $mail->FromName = 'Prefeitura Municipal de Fortaleza';
            $mail->addAddress($this->input->post('email'), $idUser->nome);   // Add a recipient
            $mail->addReplyTo('freewifi@fortaleza.ce.gov.br', 'Suporte Wifi');

            $mail->isHTML(true);                                  // Set email format to HTML
            
            $mail->CharSet = 'utf8';

            $mail->Subject = 'Confirmação de Cadastro do Wifi Praças Públicas - Prefeitura Municipal de Fortaleza';
            $mail->Body    = 'Por favor confirme o seu cadastro no Wifi Publico Gratuito clicando no link a seguir: http://wifi.fortaleza.ce.gov.br/index.php/cadastro/ativacao?id='.$idUser->id.'&email='.md5($this->input->post('email')).'<br>'
                    . '<a href="http://wifi.fortaleza.ce.gov.br/index.php/cadastro/ativacao?id='.$idUser->id.'&email='.md5($this->input->post('email')).'">Ou clique aqui.</a><br><br>'
                    . 'Please confirm your registration in the Public Wifi Free by clicking the following link: http://wifi.fortaleza.ce.gov.br/index.php/cadastro/ativacao?id='.$idUser->id.'&email='.md5($this->input->post('email')).'<br>'
                    . '<a href="http://wifi.fortaleza.ce.gov.br/index.php/cadastro/ativacao?id='.$idUser->id.'&email='.md5($this->input->post('email')).'">Or click here.</a><br>';

            if(!$mail->send()) {
                $this->load->view('banner');
                $this->load->view('menu');
                $this->load->view('modal/modalReenviarEmailConfirmacaoErro');
                $this->load->view('modal/modalSobre');
                $this->load->view('footer');
            } else {
                $this->load->view('banner');
                $this->load->view('menu');
                $this->load->view('modal/modalReenviarEmailConfirmacao');
                $this->load->view('modal/modalSobre');
                $this->load->view('footer');
            }
        }
    }
    
    private $captcha_path = 'captcha/';
    
    public function new_captcha(){
         
        $this->load->helper(array('captcha', 'file'));
        $chars = "abcdefghijklmnopqrstuvxywz";
        $word = '';
        for ($a = 0; $a <= 5; $a++) {
            $b = rand(0, strlen($chars) - 1);
            $word .= $chars[$b];
        }        
        $captcha = create_captcha(array(
            'word'        => $word,
            'img_path'    => 'captcha/',
            'img_url'    => base_url() . 'captcha/'
        ));
        $this->session->set_userdata('captcha', $captcha['word']);
        $filename = $this->captcha_path . $captcha['time'] . '.jpg';
        $this->output->set_header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
        $this->output->set_header('Cache-Control: no-cache, no-store, must-revalidate, max-age=0');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0', FALSE);
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header('Content-Type: image/jpeg');
        $this->output->set_header('Content-Length: ' . filesize($filename));
        echo read_file($filename);
    }
}