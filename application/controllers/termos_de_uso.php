<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Termos_de_uso extends CI_Controller {
    
    function __construct(){
        parent::__construct();
        $this->load->database();
    }
    
    function index(){
    	$this->load->view('banner');
        $this->load->view('menu');
        $this->load->view('termos_de_uso');
        $this->load->view('modal/modalSobre');
        $this->load->view('footer');
    }
}


?>