<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->database();
        date_default_timezone_set('America/Fortaleza');
    }

	public function index(){
        $this->load->view('banner');
        $this->load->view('menu');
        $this->load->view('mensagens');
        $this->load->view('inicio');
        $this->load->view('footer');
        $this->load->view('modal/modalSobre');
	}

}