<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Api_cogect {
    //MY_ para informar ao framework de que se trata de uma classe customizada, ou seja, não faz parte do framework. Pode ser alterada no arquivo config.php em application/config/
    public function Api_cogect() {
        require_once('api_cogect/config.php');
        require_once('api_cogect/ldapManagerAPI.php');
    }
}
?>