<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gerenciador extends CI_Model {
    public function cadastrar($dados_cadastrais){
            $this->db->insert("usuario", $dados_cadastrais); 
    }

    public function atualizar($dados_cadastrais){
            $this->db->where('email', $dados_cadastrais['email']);
            $this->db->update("usuario", $dados_cadastrais);
    }

    public function jaCadastrado($dados_cadastrais){
            $this->db->where('email', $dados_cadastrais['email']);
            $consulta = $this->db->get("usuario");
            $consulta = $consulta->result_array();

            return sizeof($consulta) > 0;
    }

    public function get_usuario($id){
            $this->db->where('id', $id);
            $usuario = $this->db->get('usuario');
            $usuario = $usuario->result_array();
            return $usuario[0];
    }
    
    public function verifica_cpf($cpf){
        $this->db->from('usuario');
        $this->db->where('documento',$cpf);
        return $this->db->get()->row();
    }

    public function verificaEmail($email=NULL){
        if($email != NULL){
            $this->db->select('usuario.*');
            $this->db->from('usuario');
            $this->db->where('email',$email);
            return $this->db->get();
        }
    }
  
    public function ativarCadastro($id=NULL,$email=NULL){
        if($id != NULL || $email != NULL){
            $this->db->select('usuario.*');
            $this->db->from('usuario');
            $this->db->where('id',$id);
            $query = $this->db->get()->row();

            $email2=$query->email;

            if(md5($email2) == $email && $query->status != '1'){
                $status = array(
                'status'=>'1'
                );
                $condicao = array(
                    'id'=>$id
                );
                $this->db->update('usuario',$status,$condicao);

                return TRUE;
            }
            else{
                return FALSE;
            }
        }
    }
    
    public function alterar_senha($id=NULL,$email=NULL,$senha=NULL){
        if($id != NULL || $email != NULL || $senha!=NULL){
            $this->db->select('usuario.*');
            $this->db->from('usuario');
            $this->db->where('id',$id);
            $query = $this->db->get()->row();
            $email2=$query->email;
            if(md5($email2) == $email){
                $status = array(
                'senha'=>$senha
                );
                $condicao = array(
                    'id'=>$id
                );
                $this->db->update('usuario',$status,$condicao);
                return TRUE;
            }
            else{
                return FALSE;
            }
        } else {
            return false;
        }
    } 

    public function login($data=NULL) {
        if($data != NULL){
            $this->db->where('documento', $data['documento']);
            $usuario = $this->db->get('usuario');
            $usuario = $usuario->result_array();

            if((sizeof($usuario) == 1) && ($data['senha'] == $usuario[0]['senha'] )) return true;
            else return false; 
        }
    }
    
    public function listagem_usuarios($qtde=0, $inicio=0) {
        if($qtde > 0) $this->db->limit($qtde, $inicio);
        $this->db->select('usuario.*,status.*');
        $this->db->from('usuario');
        $this->db->join('status','usuario.status = status.id_status');
        return $this->db->get();
    }
    
    public function verificaSeAtivo($email=NULL){
        if($email != NULL){
            $this->db->select('usuario.status');
            $this->db->from('usuario');
            $this->db->where('email',$email);
            return $this->db->get()->row();
        }
        else{
            redirect('index.php/cadastro/reenviarEmailConfirmacao');
        }
    }
}
?>
