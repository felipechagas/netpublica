var JSON_IDIOMA = null;

/* ********************************************** CHAMADAS ************************************************ */
$(document).ready( function() {
	inicializarElementos();
	carregarTraducao();
});

/* *********************************************** FUNCOES *********************************************** */
function inicializarElementos() {
	var $elementoJanela = $(window);
	var $elementoIdiomaBotao = $('#elemento-botao-idioma');
	var $elementosIdiomasItens = $('[id*="elemento-idioma"]');
	if (!testeMobile()) {
		$elementoJanela.click( function() { fecharMenuIdiomas } );
	}
	if ($elementoIdiomaBotao.length) {
		$elementoIdiomaBotao.click( alternarMenu );
	}
	if ($elementosIdiomasItens.length) {
		for (var indiceItens = $elementosIdiomasItens.length - 1; indiceItens >= 0; indiceItens--) {
			var $elementoIdiomaItem = $($elementosIdiomasItens[indiceItens]);
			if ($elementoIdiomaItem.length) {
				$elementoIdiomaItem.click( { elementoIdiomaClickado : $elementoIdiomaItem }, alterarIdioma );
			}
		}
	}
}

function carregarTraducao() {
	var idiomaStorage = window.localStorage.getItem('ctncgt-idioma');
	if (idiomaStorage) {
		modificarParaIdioma(idiomaStorage);
	} else {
		modificarParaIdioma('pt-br');
	}
	carregarTraduzindo();
}

function carregarTraduzindo() {
	var caminho = caminhoBase();
	var $elementoIdiomaEscolhido = $('#idioma-escolhido');
	if ($elementoIdiomaEscolhido.length) {
		var idioma = $elementoIdiomaEscolhido.val();
		if (idioma) {
			caminho += 'js/idioma/';
			caminho += idioma;
			caminho += '.json';
			$.getJSON(caminho, function(json) {
				JSON_IDIOMA = json;
				traduzirPlaceholders();
				traduzirRotulos();
				traduzirValores();
				traduzirDicas();
				traduzirMensagens();
			})
			.fail( function(jqxhr, textStatus, error) {
				console.log(error);
			});
		}
	}
}

function alterarIdioma(evento) {
	var $elementoIdioma = $(evento.data.elementoIdiomaClickado);
	if ($elementoIdioma.length) {
		var idioma = $elementoIdioma.attr('idioma');

		modificarParaIdioma(idioma);
		
		carregarTraduzindo();

		setTimeout( function() { fecharMenuIdiomas(); }, 0.5 * 1000);
	} 
}

function modificarParaIdioma(idioma) {
	var $elementoIdiomaEscolhido = $('#idioma-escolhido');
	var $elementoContainerIdiomas = $('#elemento-container-idiomas');
	if ($elementoContainerIdiomas.length) {
		var $elementosIdiomasImagens = $elementoContainerIdiomas.children('img[id*="elemento-idioma"]');
		if ($elementosIdiomasImagens.length) {
			for (var indiceImagem = $elementosIdiomasImagens.length - 1; indiceImagem >= 0; indiceImagem--) {
				var $elementoIdiomaImagem = $($elementosIdiomasImagens[indiceImagem]);
				if ($elementoIdiomaImagem.length) {
					if ( $elementoIdiomaImagem.attr('id') == ( 'elemento-idioma-' + idioma) ) {
						$elementoIdiomaImagem.addClass('ctncgt-idioma-detacado');
					} else {
						$elementoIdiomaImagem.removeClass('ctncgt-idioma-detacado');
					}
				}
			}
		}
	}
	if ($elementoIdiomaEscolhido.length) {
		window.localStorage.setItem('ctncgt-idioma', idioma);
		$elementoIdiomaEscolhido.val(idioma);
	}
}

function alternarMenu() {
	var $elementoContainerIdiomas = $('#elemento-container-idiomas');
	if ($elementoContainerIdiomas.length) {
		if(testeVisivel($elementoContainerIdiomas)) {
			fecharMenuIdiomas();
		} else {
			abrirMenuIdiomas();
		}
	}
}

function abrirMenuIdiomas() {
	var $elementoIdiomaBotao = $('#elemento-botao-idioma');
	var $elementoContainerIdiomas = $('#elemento-container-idiomas');
	if ($elementoIdiomaBotao.length && $elementoContainerIdiomas.length) {
		var botaoIdiomaParent = $elementoIdiomaBotao.parent();
		var alturaBotaoIdiomaParent = parseInt(botaoIdiomaParent.css('height').replace('px', ''));
		$elementoIdiomaBotao.css('height', alturaBotaoIdiomaParent + 5);
		$elementoIdiomaBotao.addClass('ctncgt-botao-idioma-destacado');
		if (!testeMobile()) {
			$elementoContainerIdiomas.slideToggle( 0.2 * 1000 );
		} else {
			$elementoContainerIdiomas.toggle();
		}
	}
}

function fecharMenuIdiomas() {
	var $elementoIdiomaBotao = $('#elemento-botao-idioma');
	var $elementoContainerIdiomas = $('#elemento-container-idiomas');
	if ($elementoIdiomaBotao.length && $elementoContainerIdiomas.length) {
		if (!testeMobile()) {
			$elementoContainerIdiomas.slideToggle( 0.2 * 1000, function() {
				$elementoIdiomaBotao.css('height', 'auto');
				$elementoIdiomaBotao.removeClass('ctncgt-botao-idioma-destacado');
			});
		} else {
			$elementoContainerIdiomas.toggle();
			$elementoIdiomaBotao.css('height', 'auto');
			$elementoIdiomaBotao.removeClass('ctncgt-botao-idioma-destacado');
		}
	}
}

function traduzirPlaceholders() {
	var arrayPlaceholders = $('[traduz-placeholder]');
	if (arrayPlaceholders.length) {
		for (var i = arrayPlaceholders.length - 1; i >= 0; i--) {
			var elementoPlaceholder = $(arrayPlaceholders[i]);
			if (elementoPlaceholder.length) {
				var placeholder = elementoPlaceholder.attr('traduz-placeholder');
				if (JSON_IDIOMA) {
					elementoPlaceholder.attr('placeholder', traduzir(placeholder));
				}
			}
		};
	}
}

function traduzirRotulos() {
	var arrayRotulos = $('[traduz-rotulo]');
	if (arrayRotulos.length) {
		for (var i = arrayRotulos.length - 1; i >= 0; i--) {
			var elementoRotulo = $(arrayRotulos[i]);
			if (elementoRotulo.length) {
				var rotulo = elementoRotulo.attr('traduz-rotulo');
				if (JSON_IDIOMA) {
					elementoRotulo.html(traduzir(rotulo));
				}
			}
		};
	}
}

function traduzirValores() {
	var arrayValores = $('[traduz-valor]');
	if (arrayValores.length) {
		for (var i = arrayValores.length - 1; i >= 0; i--) {
			var elementoValor = $(arrayValores[i]);
			if (elementoValor.length) {
				var valor = elementoValor.attr('traduz-valor');
				if (JSON_IDIOMA) {
					elementoValor.attr('value', traduzir(valor));
				}
			}
		};
	}
}

function traduzirDicas() {
	var arrayDicas = $('[traduz-dica]');
	if (arrayDicas.length) {
		for (var i = arrayDicas.length - 1; i >= 0; i--) {
			var elementoDica = $(arrayDicas[i]);
			if (elementoDica.length) {
				var valor = elementoDica.attr('traduz-dica');
				if (JSON_IDIOMA) {
					elementoDica.attr('title', traduzir(valor));
				}
			}
		};
	}
}

function traduzirMensagens() {
	var arrayMensagens = $('[traduz-mensagem]');
	if (arrayMensagens.length) {
		for (var i = arrayMensagens.length - 1; i >= 0; i--) {
			var elementoMensagem = $(arrayMensagens[i]);
			if (elementoMensagem.length) {
				var valor = elementoMensagem.attr('traduz-mensagem');
				if (JSON_IDIOMA) {
					elementoMensagem.html(traduzir(valor));
				}
			}
		};
	}
}

function traduzir(chave) {
	if (JSON_IDIOMA) {
		return JSON_IDIOMA[chave];
	}
	return chave;
}
