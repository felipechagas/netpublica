/* ********************************************** CHAMADAS ************************************************ */
$(document).ready( function() { 
	inicializarModais();
});

/* *********************************************** FUNCOES *********************************************** */
function inicializarModais() {
	var $elementosModais = $('[ctncgt-funcao-modal]');
	for (var indiceModais = $elementosModais.length - 1; indiceModais >= 0; indiceModais--) {
		var $elementoEventoModal = $($elementosModais[indiceModais]);
		if ($elementoEventoModal.length) {
			$elementoEventoModal.click( { elementoEvento : $elementoEventoModal }, eventoMostrarModal );
		}
	};
}

function eventoMostrarModal(evento) {
	var $elementoModalEvento = $(evento.data.elementoEvento);
	if ($elementoModalEvento.length) {
		mostrarModal($elementoModalEvento);
	}
}

function mostrarMensagem(titulo, mensagem, link) {
	var $elementoModal = $('#ctncgt-elemento-modal');
	var $elementoMascara = $('#ctncgt-elemento-mascara');
	if ($elementoModal.length && $elementoMascara.length) {
		var $elementoFechar = $( document.createElement('span') );
		var $elementoTitulo = $( document.createElement('span') );
		var $elementoContainerFechar = $( document.createElement('div') );
		var $elementoContainerTitulo = $( document.createElement('div') );
		var $elementoLinhaTopo = $( document.createElement('div') );
		var $elementoLinhaConteudo = $( document.createElement('div') );
		// CLASSES
		$elementoFechar.addClass('ctncgt-elemento-modal-fechar');
		$elementoTitulo.addClass('ctncgt-elemento-modal-titulo');
		$elementoContainerTitulo.addClass('ctncgt-elemento-modal-container-titulo col-xs-11 col-sm-11 col-md-11 col-lg-11');
		$elementoContainerFechar.addClass('ctncgt-elemento-modal-container-fechar col-xs-1 col-sm-1 col-md-1 col-lg-1');
		$elementoLinhaTopo.addClass('ctncgt-linha row');
		$elementoLinhaConteudo.addClass('ctncgt-linha-mensagem ctncgt-linha row');
		// APAGA CONTEUDO
		$elementoModal.empty();
		// CONTEUDOS
		$elementoTitulo.append(titulo);
		$elementoFechar.append('x');
		$elementoContainerTitulo.append($elementoTitulo);
		$elementoContainerFechar.append($elementoFechar);
		$elementoLinhaTopo.append($elementoContainerTitulo);
		$elementoLinhaTopo.append($elementoContainerFechar);
		$elementoLinhaConteudo.append(mensagem);
		// ADICIONA OS ELEMENTOS
		$elementoModal.append($elementoLinhaTopo);
		$elementoModal.append($elementoLinhaConteudo);
		// EVENTOS
		if (link.length) {
			$elementoFechar.click( function() { esconderModalRedirect(link); } );
			$elementoMascara.click( function() { esconderModalRedirect(link); } );
		} else {
			$elementoFechar.click( esconderModal );
			$elementoMascara.click( esconderModal );
		}

		if (!testeMobile()) {
			$elementoModal.slideToggle('fast', function() {
				mostrarMascara();
			});
		} else {
			$elementoModal.show();
			mostrarMascara();
		}
	}
}

function mostrarModal(elementoEvento) {
	var $elementoModal = $('#ctncgt-elemento-modal');
	var $elementoMascara = $('#ctncgt-elemento-mascara');
	var $elementoEventoConteudo = $(elementoEvento);
	if ($elementoModal.length && $elementoMascara.length && $elementoEventoConteudo.length) {
		var $elementoConteudo = $( '#' + $elementoEventoConteudo.attr('ctncgt-funcao-modal') );
		if ($elementoConteudo.length) {
			var $elementoFechar = $( document.createElement('span') );
			var $elementoContainerFechar = $( document.createElement('div') );
			var $elementoLinhaTopo = $( document.createElement('div') );
			var $elementoLinhaConteudo = $( document.createElement('div') );
			// CLASSES
			$elementoFechar.addClass('ctncgt-elemento-modal-fechar');
			$elementoContainerFechar.addClass('ctncgt-elemento-modal-container-fechar col-xs-12 col-sm-12 col-md-12 col-lg-12');
			$elementoLinhaTopo.addClass('ctncgt-linha row');
			$elementoLinhaConteudo.addClass('ctncgt-linha-conteudo ctncgt-linha row');
			// APAGA CONTEUDO
			$elementoModal.empty();
			// CONTEUDOS
			$elementoFechar.append('x');
			$elementoContainerFechar.append($elementoFechar);
			$elementoLinhaTopo.append($elementoContainerFechar);
			$elementoLinhaConteudo.append( $elementoConteudo.html() );
			// ADICIONA OS ELEMENTOS
			$elementoModal.append($elementoLinhaTopo);
			$elementoModal.append($elementoLinhaConteudo);
			// EVENTOS
			$elementoFechar.click( esconderModal );
			$elementoMascara.click( esconderModal );
			if (!testeMobile()) {
				$elementoModal.slideToggle('fast', function() {
					mostrarMascara();
				});
			} else {
				$elementoModal.show();
				mostrarMascara();
			}
		}
	}
}

function esconderModal() {
	var $elementoModal = $('#ctncgt-elemento-modal');
	if ($elementoModal.length) {
		if (!testeMobile()) {
			$elementoModal.slideToggle('fast', function() {
				$elementoModal.empty();
				esconderMascara();
			});
		} else {
			$elementoModal.hide(0, function() {
				$elementoModal.empty();
				esconderMascara();
			});
		}
	}
}

function esconderModalRedirect(link) {
	var $elementoModal = $('#ctncgt-elemento-modal');
	if ($elementoModal.length) {
		if (!testeMobile()) {
			$elementoModal.slideToggle('fast', function() {
				$elementoModal.empty();
				esconderMascara();
				redirecionar(link);
			});
		} else {
			$elementoModal.hide(0, function() {
				$elementoModal.empty();
				esconderMascara();
				redirecionar(link);
			});
		}
	}
}

function mostrarMascara() {
	var $elementoJanela = $(window);
	var $elementoDocumento = $(document);
	var $elementoMascara = $('#ctncgt-elemento-mascara');
	if ( ($elementoJanela.length && $elementoMascara.length) || ($elementoDocumento.length && $elementoMascara.length) ) {
		var body = document.body;
    	var html = document.documentElement;
		var alturaJanela = $elementoJanela.height();
		var alturaDocumento = $elementoDocumento.height();
		var alturaMaxima = Math.max(alturaJanela, alturaDocumento, 
									body.scrollHeight, body.offsetHeight,
		 							html.clientHeight, html.scrollHeight, html.offsetHeight);
		var larguraJanela = $elementoJanela.width();
		if (larguraJanela && alturaJanela) {
			$elementoMascara.show();
			$elementoMascara.width(larguraJanela);
			$elementoMascara.height(alturaMaxima);
		}
	}
}

function esconderMascara() {
	var $elementoMascara = $('#ctncgt-elemento-mascara');
	if ($elementoMascara.length) {
		$elementoMascara.hide();
	}
}

function redirecionar(link) {
	window.location.assign(link);
}
