/* ********************************************** CHAMADAS ************************************************ */
$(document).ready( function() {
	inicializarFecharMensagens();
});

/* *********************************************** FUNCOES *********************************************** */
function inicializarFecharMensagens() {
	var $elementosAFechar = $('[ctncgt-mensagem-fechar]');
	for (var indiceFechar = $elementosAFechar.length - 1; indiceFechar >= 0; indiceFechar--) {
		var $elementoAFechar = $($elementosAFechar[indiceFechar]);
		if ($elementoAFechar.length) {
			$elementoAFechar.click( { elementoClickado : $elementoAFechar }, fecharMensagem );
		}
	};
}

function fecharMensagem(evento) {
	var $elementoFecharClickado = $( evento.data.elementoClickado );
	if ($elementoFecharClickado.length) {
		var $elementoContainerClickado = $elementoFecharClickado.parent();
		if ($elementoContainerClickado.length) {
			if (!testeMobile()) {
				$elementoContainerClickado.slideToggle( 0.2 * 1000, function() {
					$elementoContainerClickado.remove();
				});
			} else {
				$elementoContainerClickado.hide(0, function() {
					$elementoContainerClickado.remove();
				});
			}
		}
	}
}
