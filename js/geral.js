var CONS_SERVIDOR = false;

/* ********************************************** CHAMADAS ************************************************ */
$('input#ctncgt-captcha').ready( function() {
    inicializarCatpcha();
});

/* *********************************************** FUNCOES *********************************************** */
function inicializarCatpcha() {
    var $elementoInputCaptcha = $('input#ctncgt-captcha');
    if ($elementoInputCaptcha.length) {
        $elementoInputCaptcha.blur( diminuirCaptcha );
    }
}

function diminuirCaptcha(evento) {
    var $elementoCaptcha = $('input#ctncgt-captcha');
    if ($elementoCaptcha.length) {
        if ($elementoCaptcha.val().length) {
            $elementoCaptcha.val( $elementoCaptcha.val().toLowerCase() );
        }
    }
}

function caminhoBase() {
	var arrayCaminho = window.location.href.split('/');
	if (arrayCaminho.length) {
            var caminho = '';
            if(CONS_SERVIDOR) {
                caminho += 'http://';
                caminho += arrayCaminho[2];
                caminho += '/';
            } else {
                caminho += 'http://';
                caminho += arrayCaminho[2];
                caminho += '/';
                caminho += arrayCaminho[3];
                caminho += '/';
            }
            return caminho;
	}
}

function caminhoDominio() {
    var arrayCaminho = window.location.href.split('/');
    if (arrayCaminho.length) {
        var caminho = '';
        caminho += arrayCaminho[2];
        return caminho;
    }
}

function testeVisivel(elemento) {
	return $(elemento).is(':visible');
}

function testeMobile() {
	return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}
